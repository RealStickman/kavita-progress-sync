# Kavita Sync

A python script to sync reading progress between two Kavita servers

## TODO

- Fix the logic for the program. Synchronisation feels extremely slow for a big library
- Threading
- More regular progress messages
- Reimplement verbose switch
- Synchronisation for Libraries with type "Comic"

## Known issues

- If a book is completely read on the source server, it will always set the book as read on the target server. The issue occurs when more, in actuality unread, chapters are present on the target server. The whole book will still be marked as read

## Installation

Download this repository and open the directory in a terminal  
Use `pip install -r requirements.txt` to install the necessary dependencies

## Usage

Call the script like the following example. Replace the "http://localhost" addresses with the addresses of your servers and the long sequences of numbers with your API key, found in your user settings.  
`./main.py --source-server http://localhost:5000 --source-apikey 8ee84af4-d13f-4bdf-96da-aa5a548add48 --source-library Manga --target-server localhost:5005 --target-apikey 5a5f52a6-ee44-473b-9989-a630c1f28536 --target-library Manga`

Full help page:

```
usage: main.py [-h] -s SOURCE_SERVER -a SOURCE_APIKEY -w SOURCE_LIBRARY -t TARGET_SERVER -k TARGET_APIKEY -l TARGET_LIBRARY

Transfer reading progress between two Kavita servers.

options:
  -h, --help            show this help message and exit
  -s SOURCE_SERVER, --source-server SOURCE_SERVER
                        Source server
  -a SOURCE_APIKEY, --source-apikey SOURCE_APIKEY
                        Source server API key
  -w SOURCE_LIBRARY, --source-library SOURCE_LIBRARY
                        Library on source server
  -t TARGET_SERVER, --target-server TARGET_SERVER
                        Target server
  -k TARGET_APIKEY, --target-apikey TARGET_APIKEY
                        Target server API key
  -l TARGET_LIBRARY, --target-library TARGET_LIBRARY
                        Library on target server
```
