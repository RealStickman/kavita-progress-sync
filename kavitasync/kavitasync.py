#!/usr/bin/env python3

import argparse

import json

import kavitapy


# NOTE function to sort series into "in progress", "completed", "unread"
# returns: list of lists containing series id and name
# Sample:
# [{'name': '86--EIGHTY-SIX', 'src_id': 10, 'tgt_id': 0},
#  {'name': 'Blue Period', 'src_id': 72, 'tgt_id': 0},
#  {'name': 'Mage & Demon Queen', 'src_id': 4, 'tgt_id': 0},
#  {'name': 'The Elusive Samurai', 'src_id': 69, 'tgt_id': 0},
#  {'name': 'your name.', 'src_id': 41, 'tgt_id': 0}]
def fn_series_id_progress(lib_series):
    series_inprogress, series_completed, series_unread = [], [], []
    for series in lib_series:
        # TODO rewrite src --> source, tgt --> target
        if series["pages"] == series["pagesRead"]:
            # print(series["name"] + " has been completed")
            series_completed.append(
                {"name": series["name"], "src_id": series["id"], "tgt_id": 0}
            )
        elif series["pagesRead"] == 0:
            # print(series["name"] + " has not been started")
            series_unread.append(
                {"name": series["name"], "src_id": series["id"], "tgt_id": 0}
            )
        elif series["pages"] > series["pagesRead"]:
            # print(series["name"] + " has been started")
            series_inprogress.append(
                {"name": series["name"], "src_id": series["id"], "tgt_id": 0}
            )

    return (series_inprogress, series_completed, series_unread)


# NOTE takes in lists of series and searches for them in all the series on the target system
# All series will now have tgt_id filled as well
# [{'name': '86--EIGHTY-SIX', 'src_id': 10, 'tgt_id': 186},
#  {'name': 'Blue Period', 'src_id': 72, 'tgt_id': 180},
#  {'name': 'Mage & Demon Queen', 'src_id': 4, 'tgt_id': 152},
#  {'name': 'The Elusive Samurai', 'src_id': 69, 'tgt_id': 120},
#  {'name': 'your name.', 'src_id': 41, 'tgt_id': 132}]
def fn_target_search_series(series):
    for tgt_series in target_all_series:
        if series["name"] == tgt_series["name"]:
            series["tgt_id"] = tgt_series["id"]


"""
def fn_target_search_series(target_library, source_series):
    for series in source_series:
        found_list = target_kavita.library_search(series["name"])
        # go through results and match the best one
        for found in found_list["series"]:
            if (
                series["name"] == found["name"]
                and target_library == found["libraryName"]
            ):
                if verbose:
                    print("Searched " + series["name"] + " Found " + found["name"])
                series["tgt_id"] = found["seriesId"]
"""


def main():
    # NOTE argument parsing
    parser = argparse.ArgumentParser(
        description="Transfer reading progress between two Kavita servers."
    )  # pylint: disable=line-too-long

    # source
    parser.add_argument(
        "-s", "--source-server", required=True, type=str, help="Source server"
    )
    parser.add_argument(
        "-a", "--source-apikey", required=True, type=str, help="Source server API key"
    )
    parser.add_argument(
        "-w",
        "--source-library",
        required=True,
        type=str,
        help="Library on source server",
    )

    # target
    parser.add_argument(
        "-t", "--target-server", required=True, type=str, help="Target server"
    )
    parser.add_argument(
        "-k", "--target-apikey", required=True, type=str, help="Target server API key"
    )
    parser.add_argument(
        "-l",
        "--target-library",
        required=True,
        type=str,
        help="Library on target server",
    )

    # verbose output
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="Increase output verbosity"
    )

    args = parser.parse_args()

    verbose = args.verbose

    if verbose:
        print("Showing verbose output")

    source_url = args.source_server
    source_apikey = args.source_apikey
    source_library = args.source_library

    target_url = args.target_server
    target_apikey = args.target_apikey
    target_library = args.target_library

    source_all_libraries = json.loads(
        kavitapy.Library(source_url, source_apikey).content
    )

    target_all_libraries = json.loads(
        kavitapy.Library(target_url, target_apikey).content
    )

    # check if both libraries have the same type

    # get type for source library
    for library in source_all_libraries:
        if source_library == library["name"]:
            source_library_type = library["type"]
            source_library_id = library["id"]

    # get type for target library
    for library in target_all_libraries:
        if target_library == library["name"]:
            target_library_type = library["type"]
            target_library_id = library["id"]

    # check if both libraries are of the same type
    if source_library_type != target_library_type:
        prints("The source and target libraries need to be of the same type")
        exit(1)

    # get all series in library on source
    source_all_series = json.loads(
        kavitapy.Series(url=source_url, apikey=source_apikey)
        .default(library=source_library_id)
        .content
    )

    # get all series in library on target
    # FIXME global variable
    global target_all_series
    target_all_series = json.loads(
        kavitapy.Series(url=target_url, apikey=target_apikey)
        .default(library=target_library_id)
        .content
    )

    # TODO series_completed has issues if less chapters are available on the source server
    # than on the target server
    # it might be better (more accurate) to remove series_completed
    # and only handle "unread" and "inprogress"
    (
        source_series_inprogress,
        source_series_completed,
        source_series_unread,
    ) = fn_series_id_progress(source_all_series)
    print(
        "Found "
        + str(len(source_series_inprogress))
        + " books in progress, "
        + str(len(source_series_completed))
        + " books completed and "
        + str(len(source_series_unread))
        + " unread on source server."
    )

    # search series from the source on the target server and get their ids
    for series in source_series_inprogress:
        fn_target_search_series(series)
    for series in source_series_completed:
        fn_target_search_series(series)
    for series in source_series_unread:
        fn_target_search_series(series)

    # TODO rewrite variables above to remove this
    series_inprogress = source_series_inprogress
    series_completed = source_series_completed
    series_unread = source_series_unread

    # set series read on the target
    kavitapy.Reader(target_url, target_apikey).mark_multiple_series_read(
        [list_completed["tgt_id"] for list_completed in series_completed]
    )

    # set series unread on the target
    kavitapy.Reader(target_url, target_apikey).mark_multiple_series_unread(
        [list_unread["tgt_id"] for list_unread in series_unread]
    )

    # NOTE library types
    # 0: Manga
    # 1: Comic
    # 2: Book

    # NOTE mark progress for manga libraries
    if source_library_type == 0:
        for series in series_inprogress:
            source_progress = json.loads(
                kavitapy.Reader(source_url, source_apikey)
                .continue_point(series["src_id"])
                .content
            )
            source_chapter = json.loads(
                kavitapy.Reader(source_url, source_apikey)
                .get_progress(source_progress["id"])
                .content
            )
            source_volumes = json.loads(
                kavitapy.Series(source_url, source_apikey)
                .volumes(series["src_id"])
                .content
            )
            target_volumes = json.loads(
                kavitapy.Series(target_url, target_apikey)
                .volumes(series["tgt_id"])
                .content
            )
            # create a list of chapter ids across the source and target server
            chapters = []
            for volume in source_volumes:
                for chapter in volume["chapters"]:
                    chapters.append(
                        {
                            "name": chapter["number"],
                            "pages": chapter["pages"],
                            "src_id": chapter["id"],
                            "tgt_vol": 0,
                            "tgt_id": 0,
                        }
                    )

            for volume in target_volumes:
                for chapter in volume["chapters"]:
                    for chpt in chapters:
                        if chpt["name"] == chapter["number"]:
                            chpt["tgt_id"] = chapter["id"]
                            chpt["tgt_vol"] = chapter["volumeId"]

            for chapter in chapters:
                # NOTE explicit float for correct number comparison
                # and handling .x chapters
                if float(chapter["name"]) < float(source_progress["number"]):
                    # save as fully read
                    kavitapy.Reader(target_url, target_apikey).progress(
                        series=series["tgt_id"],
                        volume=chapter["tgt_vol"],
                        chapter=chapter["tgt_id"],
                        page=chapter["pages"],
                    )
                elif float(chapter["name"]) == float(source_progress["number"]):
                    # set progress to page number
                    kavitapy.Reader(target_url, target_apikey).progress(
                        series=series["tgt_id"],
                        volume=chapter["tgt_vol"],
                        chapter=chapter["tgt_id"],
                        page=source_progress["pagesRead"],
                        bookscroll=source_chapter["bookScrollId"],
                    )
                elif float(chapter["name"]) > float(source_progress["number"]):
                    # set progress to 0
                    kavitapy.Reader(target_url, target_apikey).progress(
                        series=series["tgt_id"],
                        volume=chapter["tgt_vol"],
                        chapter=chapter["tgt_id"],
                        page=0,
                    )

    # TODO continue with book library
    elif source_library_type == 2:
        for series in series_inprogress:
            source_progress = json.loads(
                kavitapy.Reader(source_url, source_apikey)
                .continue_point(series["src_id"])
                .content
            )
            source_chapter = json.loads(
                kavitapy.Reader(source_url, source_apikey)
                .get_progress(source_progress["id"])
                .content
            )
            source_volumes = json.loads(
                kavitapy.Series(source_url, source_apikey)
                .volumes(series["src_id"])
                .content
            )
            target_volumes = json.loads(
                kavitapy.Series(target_url, target_apikey)
                .volumes(series["tgt_id"])
                .content
            )
            # NOTE ebooks don't have distinct chapter numbers
            # we need to set progress on the volume and its first chapter instead
            source_vol = json.loads(
                kavitapy.Series(source_url, source_apikey)
                .volume(source_progress["volumeId"])
                .content
            )

            # create a list of volume ids across the source and target server
            volumes = []
            for volume in source_volumes:
                volumes.append(
                    {
                        "name": volume["number"],
                        "pages": volume["pages"],
                        "src_id": volume["id"],
                        "tgt_vol": 0,
                        "tgt_ch": 0,
                    }
                )

            for volume in target_volumes:
                for vol in volumes:
                    if vol["name"] == volume["number"]:
                        vol["tgt_vol"] = volume["id"]
                        vol["tgt_ch"] = volume["chapters"][0]["id"]

            for volume in volumes:
                # NOTE explicit float so the numbers are compared properly
                # and special chapters work
                if float(volume["name"]) < float(source_vol["number"]):
                    # save as fully read
                    kavitapy.Reader(target_url, target_apikey).progress(
                        series=series["tgt_id"],
                        volume=volume["tgt_vol"],
                        chapter=volume["tgt_ch"],
                        page=volume["pages"],
                    )
                elif float(volume["name"]) == float(source_vol["number"]):
                    # set progress to page number
                    kavitapy.Reader(target_url, target_apikey).progress(
                        series=series["tgt_id"],
                        volume=volume["tgt_vol"],
                        chapter=volume["tgt_ch"],
                        page=source_progress["pagesRead"],
                        bookscroll=source_chapter["bookScrollId"],
                    )
                elif float(volume["name"]) > float(source_vol["number"]):
                    # save as fully read
                    kavitapy.Reader(target_url, target_apikey).progress(
                        series=series["tgt_id"],
                        volume=volume["tgt_vol"],
                        chapter=volume["tgt_ch"],
                        page=0,
                    )

    else:
        print("Library type is not yet supported")
        exit(1)

    (
        target_series_inprogress,
        target_series_completed,
        target_series_unread,
    ) = fn_series_id_progress(target_all_series)
    print(
        "Found "
        + str(len(target_series_inprogress))
        + " books in progress, "
        + str(len(target_series_completed))
        + " books completed and "
        + str(len(target_series_unread))
        + " unread on target server."
    )


# execute main block
if __name__ == "__main__":
    main()
